# openrc-calm-init

Репозиторий с архивами протестированных и/или тестируемых версий системы инициализации [OpenRC](https://github.com/OpenRC/openrc).

## Versions

| OpenRC ver. | Calmira GNU/Linux-libre ver. |
|-------------|------------------------------|
| `0.46` | `2.0` (`2023.1`) |